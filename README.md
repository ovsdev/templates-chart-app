# templates-chart-app #

This is a template of an Helm chart used to build and deploy a web app

## Chart Setup ##

To allow cicd process to read your helm chart put the ./chart folder in the root of your repositoy

In order to integrate this helm chart in your application you have to configure the following files:

* /chart/Chart.yaml: file that describe the helm chart of the application
  * edit name and description fields
  * edit minor or major version if you want to update the app minor or major
  * do not edit the patch part of the version or appVersion because they are dynamically assigned during cicd process

* /templates/filter.yaml
  * You have to create the sealed secret for you keycloak client and replace the one used in this template as an example

## Templates ##

The templates folder contains all the resources that will be deployed during the cicd process

* **deployment**
  * this describe how the docker image of the app will be deployed
  * add here your env variable necessary for your application
* **filter**
  * ambassador resources used to filter the access to the app
* **host**
  * ambassador resource that allow the app to be exposed on the internet
* **ingress**
  * kubernetes resources used to expose the app during the preview phase
* **mapping**
  * ambassador resource necessary to link the ambassador host to the application app (kubernetes)
* **app**
  * kubernetes resources that allow pods created with the deployment to be reachable from other resources

## Values ##

* **values.yaml**: should contain only default values valid in preview, staging and production environment
* **values-preview.yaml**: values valid only in preview environment
* **values-staging.yaml**: values valid only in staging environment
* **values-production.yaml**: values valid only in production environment

The following values of the .yaml files in the /templates folder are setup during the cicd phases.
Removing or editing them could cause build or deployment failure of you project.

* {{ .Values.host.url }}
* {{ .Values.host.email }}
* {{ .Values.ingress.appDomain }}
* {{ .Values.ingress.subdomain }}
* {{ .Values.ingress.tlsSecret }}
* {{ .Values.image.repository }}
* {{ .Values.image.tag }}
